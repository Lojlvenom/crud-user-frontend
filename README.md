# CRUD User

Para levantar o container utilize o seguinte comando 

```
docker-compose up && docker-compose rm -fvs  
```

Execute o seguinte comando depois de subir o container

```
docker exec frontend npx json-server -H 0.0.0.0 --watch dados.json
```

URL da interface em Angular

```
http://localhost:4200
```

URL dos dados mockados em JSON

```
http://localhost:3000/usuarios
```
