import { EstudantesFormComponent } from './estudantes-form/estudantes-form.component';
import { ListaEstudantesComponent } from './lista-estudantes/lista-estudantes.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ListaEstudantesComponent,
  },
  {
    path: 'criar',
    component: EstudantesFormComponent,
  },
  {
    path: 'editar/:id',
    component: EstudantesFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class usuariosRoutingModule {}
