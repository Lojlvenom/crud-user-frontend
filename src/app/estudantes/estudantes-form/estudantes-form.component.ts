import { usuario } from 'src/app/estudantes/lista-estudantes/usuario';
import { AlertModalService } from '../../shared/alert-modal.service';
import { EstudantesService } from '../estudantes.service';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validator,
  ValidatorFn,
  Validators,
  FormControl,
} from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap, first } from 'rxjs/operators';

@Component({
  selector: 'app-usuarios-form',
  templateUrl: './estudantes-form.component.html',
  styleUrls: ['./estudantes-form.component.css'],
})
export class EstudantesFormComponent implements OnInit {
  // form!: FormGroup;
  submitted = false;
  id?: number;
  usuario: usuario = {
    nome: '',
    matricula: '',
  };

  form!: FormGroup;

  ngOnInit(): void {
    this.route.params.subscribe((value) => (this.id = value.id));
    if (this.id) {
      this.service
        .loadById(this.id)
        .pipe(first())
        .subscribe((x) => {
          this.usuario = x;
          x.matricula = x.matricula?.toString();
          this.form.patchValue(x);
        });
    }

    this.form = new FormGroup({
      nome: new FormControl(this.usuario.nome, [
        Validators.required,
        Validators.minLength(2),
      ]),
      matricula: new FormControl(this.usuario.matricula, [
        Validators.required,
        Validators.minLength(1),
      ]),
    });
  }

  get nome() {
    return this.form.get('nome')!;
  }

  get matricula() {
    return this.form.get('matricula')!;
  }

  constructor(
    private service: EstudantesService,
    private location: Location,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  OnSubmit() {
    // this.submitted = true;
    if (this.form.valid && !this.id) {
      this.usuario = this.form.value;
      this.service.save(this.usuario).subscribe((success) => {
        console.log('user', success);
        this.router.navigate(['/', '/']);
      });
    } else {
      this.usuario = {
        id: this.id,
        ...this.form.value,
      };
      this.service.save(this.usuario).subscribe((success) => {
        console.log('user', success);
        this.router.navigate(['/', '/']);
      });
    }
  }

  OnCancel() {
    this.submitted = false;
    this.form.reset();
    this.location.back();
  }
}
