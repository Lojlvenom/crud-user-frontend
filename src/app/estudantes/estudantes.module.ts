import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { usuariosRoutingModule } from './estudantes-routing.module';
import { ListaEstudantesComponent } from './lista-estudantes/lista-estudantes.component';
import { EstudantesFormComponent } from './estudantes-form/estudantes-form.component';

@NgModule({
  declarations: [ListaEstudantesComponent, EstudantesFormComponent],
  imports: [CommonModule, usuariosRoutingModule, ReactiveFormsModule],
})
export class usuariosModule {}
