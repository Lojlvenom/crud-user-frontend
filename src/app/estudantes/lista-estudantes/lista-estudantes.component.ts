import { EstudantesService } from '../estudantes.service';
import { preserveWhitespacesDefault } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { usuario } from './usuario';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertModalService } from 'src/app/shared/alert-modal.service';
import { exhaustMap } from 'rxjs/operators';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-estudantes.component.html',
  styleUrls: ['./lista-estudantes.component.scss'],
  preserveWhitespaces: true,
})
export class ListaEstudantesComponent implements OnInit {
  usuarios$!: Observable<usuario[]>;
  usuarioSelecionado!: usuario;

  constructor(
    private service: EstudantesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.service.listar().subscribe(dados => this.usuarios = dados);
    this.usuarios$ = this.service.listar();
  }

  OnEdit(id) {
    this.router.navigate(['editar', id], { relativeTo: this.route });
  }

  OnDelete(usuario) {
    this.service
      .remove((this.usuarioSelecionado = usuario))
      .subscribe((success) => (this.usuarios$ = this.service.listar()));
  }
}
