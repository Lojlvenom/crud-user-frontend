import { usuario } from './lista-estudantes/usuario';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap, delay, take, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EstudantesService {
  private readonly API = `${environment.apiUrl}student`;

  constructor(private http: HttpClient) {}

  listar() {
    return this.http.get<usuario[]>(this.API).pipe(delay(0), tap());
  }

  loadById(id) {
    return this.http.get<usuario>(`${this.API}/${id}`).pipe(take(1));
  }

  private create(estudante) {
    return this.http.post(this.API, estudante).pipe(take(1));
  }

  private update(estudante) {
    return this.http
      .patch(`${this.API}/${estudante.id}`, estudante)
      .pipe(take(1));
  }

  save(estudante) {
    console.log('id', estudante);

    if (estudante.id) {
      return this.update(estudante);
    }
    return this.create(estudante);
  }

  remove(estudante) {
    return this.http
      .delete(`${this.API}/${estudante.id}`, estudante)
      .pipe(take(1));
  }
}
