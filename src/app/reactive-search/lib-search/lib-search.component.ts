import { environment } from './../../../environments/environment.prod';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { tap, map, filter, distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-lib-search',
  templateUrl: './lib-search.component.html',
  styleUrls: ['./lib-search.component.scss']
})
export class LibSearchComponent implements OnInit {
  queryField = new FormControl();
  readonly SEARCH_URL = "http://localhost:3000/usuarios";
  results$!: Observable<any>;
  total!: number;
  readonly FIELDS = 'id,nome,login';

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.results$ = this.http.get(this.SEARCH_URL)
    this.results$.subscribe((val) =>{
      this.total = val.length
    })

    console.log(this.http.get(this.SEARCH_URL))
  
  }

  onSearch() {
    let value = this.queryField.value;
    if(value && (value = value.trim()) !== ''){
      this.results$ = this.http.get(this.SEARCH_URL) 
      this.results$.subscribe((val)=> {
        console.log(val);
      });
    }
    this.results$.subscribe((val)=> {
      let filtered = val.filter(values => values.id == value || values.nome.includes(value) || values.login.includes(value))
      this.total = filtered.length 
      console.log(JSON.stringify(filtered))
      this.results$ = of(filtered)
      });
    
  }
}
