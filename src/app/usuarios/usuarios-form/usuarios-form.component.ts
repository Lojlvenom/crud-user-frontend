import { usuario } from 'src/app/usuarios/lista-usuarios/usuario';
import { AlertModalService } from './../../shared/alert-modal.service';
import { usuariosService } from './../usuarios.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validator, ValidatorFn, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-usuarios-form',
  templateUrl: './usuarios-form.component.html',
  styleUrls: ['./usuarios-form.component.css'],
})

export class UsuariosFormComponent implements OnInit {
  form!: FormGroup;
  submitted = false;
  /*usuario: usuario = {
    id: 1,
    nome: "",
    email: "",
    senha: "",
    login: ""
  };*/

  ngOnInit(): void {

    const usuario:usuario = this.route.snapshot.data['usuario'];
    this.form = new FormGroup({
      id: new FormControl([usuario.id]),
      nome: new FormControl (usuario.nome, [
        Validators.required,
        Validators.maxLength(50)
      ]),
      email: new FormControl (usuario.email, [
        Validators.required,
        Validators.maxLength(40),
        Validators.email
      ]),
      login: new FormControl (usuario.login, [
        Validators.required,
        Validators.maxLength(30),
      ]),
      senha: new FormControl (usuario.senha, [
        Validators.required,
        Validators.maxLength(20),
      ]),
    })
  }


  get nome() {
    return this.form.get('nome')!
  }

  get senha() {
    return this.form.get('senha')!
  }

  get login() {
    return this.form.get('login')!
  }

  get email() {
    return this.form.get('email')!
  }


  constructor(
    private service: usuariosService,
    private location: Location,
    private route: ActivatedRoute
  ) {}

  OnSubmit() {
    this.submitted = true;
    console.log(this.form.value);
    if (this.form.valid) {
      console.log('submit');

      if (this.form.value.id) {
        console.log('safe');
      }

      this.service.save(this.form.value).subscribe(
        success => {
          console.log('foi att');
          this.location.back();
        },
        (err) => console.error(err),
          () => console.log('teste0')
      );

     /* if (this.form.value.id) {
        this.service.update(this.form.value).subscribe(
          (sucess) => {
            console.log('foi att');
            this.location.back();
          },
          (err) => console.error(err),
          () => console.log('teste1')
        );
      } else {
        this.service.create(this.form.value).subscribe(
          (sucess) => {
            console.log('foi');
            this.location.back();
          },
          (err) => console.error(err),
          () => console.log('teste')
        );
      } */
    }
  }

  OnCancel() {
    this.submitted = false;
    this.form.reset();
    this.location.back();
    // console.log('Cancel');
  }
}
