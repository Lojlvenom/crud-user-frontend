import { usuario } from './lista-usuarios/usuario';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { tap, delay, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class usuariosService {

  private readonly API = `${environment.apiUrl}usuarios`;

  constructor(private http: HttpClient) { }

  listar() {
    return this.http.get<usuario[]>(this.API)
    .pipe(
      delay(1000),
      tap(console.log)
    )
  }

  loadById(id) {
    console.log(id, "usuario loadbyID")
    return this.http.get<usuario>(`${this.API}/${id}`).pipe(take(1));
  }

  private create(usuario) {
    return this.http.post(this.API, usuario).pipe(take(1));
  }

  private update(usuario) {
    return this.http.put(`${this.API}/${usuario.id}`, usuario).pipe(take(1));
  }

  save(usuario) {
    if (usuario.id && usuario.id[0] != null) {
      return this.update(usuario);
    }
    usuario.id = usuario.id[0]
    return this.create(usuario);
  }

  remove(usuario) {
    return this.http.delete(`${this.API}/${usuario.id}`, usuario).pipe(take(1));
  }
}
