FROM node:14

WORKDIR /app

COPY package*.json ./

RUN npm install -g @angular/cli
RUN npm i @angular-devkit/build-angular
RUN npm install

EXPOSE 4200
EXPOSE 3000

CMD ["npm", "start"]
